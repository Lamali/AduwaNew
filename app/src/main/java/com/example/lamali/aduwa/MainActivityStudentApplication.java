package com.example.lamali.aduwa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivityStudentApplication extends AppCompatActivity {
    private Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_student_application);

        b1=(Button) findViewById(R.id.btnSubmit);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCanteenMeal();
            }
        });

    }
    public void openCanteenMeal(){
        Intent intent=new Intent(this,SelectCanteenActivity.class);
        startActivity(intent);
    }
}
