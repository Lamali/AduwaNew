package com.example.lamali.aduwa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SignInSignUpActivity extends AppCompatActivity {

     private Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_sign_up);


       b1 = (Button) findViewById(R.id.btnSignUp);
        b2=(Button) findViewById(R.id.btnSignIn);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStaffOwnerStudent();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignInActivity();
            }
        });

        }

        public void openStaffOwnerStudent(){
            Intent intent1=new Intent(this,MainActivityStaffOwnerStudent.class);
            startActivity(intent1);
        }
        public void openSignInActivity(){
            Intent intent2=new Intent(this,SingInActivity.class);
            startActivity(intent2);
        }

}









